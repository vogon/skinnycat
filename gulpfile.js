let gulp = require("gulp");
let sourcemaps = require("gulp-sourcemaps");
let tsc = require("gulp-typescript");
let webpack = require("webpack-stream");

function client() {
    return gulp
        .src("client/**/*.ts")
        .pipe(webpack(require("./src/client/webpack.config.js")))
        .pipe(gulp.dest("public/build"));
}

function server() {
    var project = tsc.createProject("./src/server/tsconfig.json");

    return (
        gulp
            .src(["src/server/**/*.ts"])
            .pipe(sourcemaps.init())
            .pipe(project())
            // https://github.com/ivogabe/gulp-typescript/issues/538#issuecomment-354152138
            // is the post that finally gave me the information I needed to fix the
            // source map paths; for some reason, it only works with discrete map
            // files and not inline map files though
            .pipe(sourcemaps.write(".", { sourceRoot: "../../src/" }))
            .pipe(gulp.dest("release/server"))
    );
}

const build = gulp.parallel(
    client,
    server
);

exports.default = build;