import express from "express";
import * as http from "http";
import io from "socket.io";

export const app = express();

app.use("/static", express.static("public"));

app.get("/", (req, res) => {
    return res.redirect(303, "/static/index.html");
})

const server = http.createServer(app);

const socket = new io(server);

let clients: io.Socket[] = [];

socket.on("connection", (client) => {
    console.log("a user connected");
    clients.push(client);

    client.on("message", (msg) => {
        console.log(`<${msg.from}> ${msg.text}`);
        
        for (let otherClient of clients) {
            otherClient.emit("message", msg);
        }
    });

    client.on("disconnect", () => {
        clients = clients.splice(clients.indexOf(client), 1);
    }); 
});

server.listen(8080);